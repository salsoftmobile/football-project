﻿using SmartMenuManagement.Scripts;
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Assets.FootballGameEngine_Indie_.Scripts.UI.Menus.UtilityMenu.MainMenu
{
    [Serializable]
    public class MobileBtnInputs: BMenu
    {
        //[SerializeField]
        //public Button _shortBtn;

        //[SerializeField]
        //private Button _LongBtn;

        //[SerializeField]
        //private Button _SprintBtn;

        //[SerializeField]
        //private Button _BigShoot;

        [SerializeField]
        public GameObject _Joystick;
        public void Init()
        {
            _Joystick.SetActive(true);
        }

     
            //public void Init(bool isBackButtonActive, bool isContinueButtonActive, string heading/*, UnityAction onClickBackButton = null, UnityAction onClickContinueButton = null*/)
            //{
            //    //// remove any listeners
            //    //_shortBtn.onClick.RemoveAllListeners();
            //    //_btnContinue.onClick.RemoveAllListeners();

            //    //// check if we have something
            //    //bool onClickBackButtonFlag = onClickBackButton != null;
            //    //if (onClickBackButtonFlag)
            //    //    _btnBack.onClick.AddListener(onClickBackButton);

            //    //bool onClickContinueButtonFlag = onClickContinueButton != null;
            //    //if (onClickContinueButtonFlag)
            //    //    _btnContinue.onClick.AddListener(onClickContinueButton);

            //    //// enable the appropriate menus
            //    //_btnBack.gameObject.SetActive(isBackButtonActive);
            //    //_btnContinue.gameObject.SetActive(isContinueButtonActive);

            //    //// set the header
            //    //_txtHeading.text = heading.ToUpper();
            //}
        }
}
