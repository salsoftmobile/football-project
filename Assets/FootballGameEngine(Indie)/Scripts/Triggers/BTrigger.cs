﻿using System;
using UnityEngine;
using System.Collections;
using Assets.FootballGameEngine_Indie.Scripts.Entities;

namespace Assets.FootballGameEngine_Indie_.Scripts.Triggers
{
    public class BTrigger : MonoBehaviour
    {
        public Action OnCollidedWithBall;

        public virtual void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Ball")
            {
                //invoke that the wall has collided with the ball
                Action temp = OnCollidedWithBall;
                if (temp != null)
                    temp.Invoke();

                //StartCoroutine(WaitThenPlaceBall(other.gameObject));


            }
        }
        public virtual void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.tag == "Ball")
            {
                //invoke that the wall has collided with the ball
                Action temp = OnCollidedWithBall;
                if (temp != null)
                    temp.Invoke();
            }
        }

        //IEnumerator
        //public IEnumerator WaitThenPlaceBall(GameObject other)
        //{
        //    Debug.Log("BTRIGGER");
        //    yield return new WaitForSeconds(1f);

        //    other.gameObject.GetComponent<Ball>().DisablePhysics();
        //    other.gameObject.GetComponent<Ball>().EnablePhysics();

        //   // other.transform.position = pos.transform.position;


        //}
    }
}
