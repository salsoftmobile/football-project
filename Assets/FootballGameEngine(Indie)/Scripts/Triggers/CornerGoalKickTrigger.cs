﻿using System;
using Assets.FootballGameEngine_Indie_.Scripts.States.Entities.PlayerStates.GoalKeeperStates.PutBallBackIntoPlay.SubStates;
using RobustFSM.Base;
using UnityEngine;
using System.Collections;
using Assets.FootballGameEngine_Indie.Scripts.Entities;

using Assets.FootballGameEngine_Indie.Scripts.StateMachines.Entities;


namespace Assets.FootballGameEngine_Indie_.Scripts.Triggers
{
    
    public class CornerGoalKickTrigger : BTrigger
    {
        public Transform pos;
       // public GameObject BallParent;

        public delegate void CollidedWithBall(Vector3 position);
        public new CollidedWithBall OnCollidedWithBall;

        public override void OnTriggerEnter(Collider other)
        {
            base.OnTriggerEnter(other);
            
            if (other.tag == "Ball")
            {
                Debug.Log("Set Ball postion for kick 1");
                // cache the collision position
                //CollidedWithBall temp = OnCollidedWithBall;
                //if (temp != null)
                //{
                    

                //    //temp.Invoke(other.transform.position);
                //    Debug.Log("goal kick");
                //}

                StartCoroutine(WaitThenPlaceBall(other.gameObject));
            }
        }
        
        public override void OnCollisionEnter(Collision other)
        {
            base.OnCollisionEnter(other);
            
            if (other.gameObject.tag == "Ball")
            {
                Debug.Log("Set Ball postion for kick col");
                // cache the collision position
                CollidedWithBall temp = OnCollidedWithBall;
                if (temp != null)
                {
                    Debug.Log(other);
                    temp.Invoke(other.transform.position);
                    Debug.Log("goal kick col");

                

                  // StartCoroutine(WaitThenPlaceBall(other.gameObject)); 
                }
              

            }
        }

        public IEnumerator WaitThenPlaceBall(GameObject other)
        {
            Debug.Log("CornerGoalKickTrigger");

            yield return new WaitForSeconds(1f);
            //other.gameObject.GetComponent<Ball>().DetachBallToParent();
            other.gameObject.GetComponent<Ball>().DisablePhysics();
            other.gameObject.GetComponent<Ball>().EnablePhysics();

            other.transform.position = pos.transform.position;
    

        }

        
    }
}
