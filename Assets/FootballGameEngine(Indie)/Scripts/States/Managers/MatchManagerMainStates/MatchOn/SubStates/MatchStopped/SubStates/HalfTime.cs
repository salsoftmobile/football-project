﻿using System;
using Assets.FootballGameEngine_Indie.Scripts.Managers;
using Assets.FootballGameEngine_Indie.Scripts.StateMachines.Managers;
using Assets.FootballGameEngine_Indie.Scripts.Utilities;
using RobustFSM.Base;
using UnityEngine;
using static Assets.FootballGameEngine_Indie.Scripts.Managers.MatchManager;

namespace Assets.FootballGameEngine_Indie.Scripts.States.Managers.MatchManagerMainStates.MatchOn.SubStates.MatchStopped.SubStates
{
    public class HalfTime : BState
    {
        public override void Enter()
        {
            base.Enter();
            Debug.Log("half 1");
            //listen to instructions to go to second half
            Owner.OnContinueToSecondHalf += Instance_OnContinueToSecondHalf;

            //raise the event that I have entered the half-time state
            RaiseTheHalfTimeStartEvent();
            Debug.Log("half 2");
        }

        public override void Exit()
        {
            base.Exit();
            Debug.Log("half 3");
            //stop listening to instructions to go to second half
            Owner.OnContinueToSecondHalf -= Instance_OnContinueToSecondHalf;

            //raise the event that I have exited the half-time state
            ActionUtility.Invoke_Action(Owner.OnExitHalfTimeState);
            Debug.Log("half 4");
        }

        /// <summary>
        /// Raises the half start event
        /// </summary>
        public void RaiseTheHalfTimeStartEvent()
        {
            Debug.Log("half 5");
            //prepare an empty string
            string message = string.Format("{0} {1}-{2} {3}",
                Owner.TeamAway.TeamData.Name,
                Owner.TeamAway.Goals,
                Owner.TeamHome.Goals,
                Owner.TeamHome.TeamData.Name);
            Debug.Log("half 6");
            //raise the event
            ActionUtility.Invoke_Action(message, Owner.OnEnterHalfTimeState);
            Debug.Log("half 7");
        }

        private void Instance_OnContinueToSecondHalf()
        {
            Debug.Log("half 8");
            Machine.ChangeState<SwitchSides>();
        }

        /// <summary>
        /// Returns the owner of this instance
        /// </summary>
        public MatchManager Owner
        {
            get
            {
                return ((MatchManagerFSM)SuperMachine).Owner;
            }
        }
    }
}
