﻿using Assets.FootballGameEngine_Indie.Scripts.Managers;
using Assets.FootballGameEngine_Indie_.Scripts.Managers;
using Assets.FootballGameEngine_Indie_.Scripts.States.Managers.GameManagerStates.GameOnState.SubStates.HalfTime.SubStates;
using RobustFSM.Base;
using UnityEngine;

namespace Assets.FootballGameEngine_Indie_.Scripts.States.Managers.GameManagerStates.GameOnState.SubStates.HalfTime.MainState
{
    public class HalfTimeState : BHState
    {
        public static string GoalsFoundTemp = /*MatchManager.Instance.AwayTeamData.ShortName.ToString() +*/ "s1 4-4 s2";

        public override void AddStates()
        {
            base.AddStates();

            // addt states
            AddState<HalfTimeTeamManagementState>();
            AddState<ShowHalfTimeMenuState>();

            // set initial state
            SetInitialState<ShowHalfTimeMenuState>();
        }

        public override void Enter()
        {

     


        // disanle all children
        GraphicsManager.Instance.GameOnMainMenu
               .HalfTimeMainMenu
               .DisableChildren();

            // enable the half-timemenu
            GraphicsManager.Instance.GameOnMainMenu
                .HalfTimeMainMenu
                .Root
                .SetActive(true);

            // Mine
            Debug.Log("Half Enter: " + GraphicsManager.Instance
                .GameOnMainMenu
                .MatchInPlayMainMenu
                .MatchPlayMenu
                .TxtScores
                .text);

            GoalsFoundTemp = GraphicsManager.Instance
                .GameOnMainMenu
                .MatchInPlayMainMenu
                .MatchPlayMenu
                .TxtScores
                .text;
            GraphicsManager.Instance.mobileBtnInputs._Joystick.SetActive(false); // mine

            // run the enter
            base.Enter();
        }

        public override void Exit()
        {
            base.Exit();

            // disable the half-timemenu
            GraphicsManager.Instance.GameOnMainMenu
                .HalfTimeMainMenu
                .Root
                .SetActive(false);

            //Mine
            Debug.Log("Half Exit: " + GraphicsManager.Instance
                .GameOnMainMenu
                .MatchInPlayMainMenu
                .MatchPlayMenu
                .TxtScores
                .text);

            GraphicsManager.Instance
                .GameOnMainMenu
                .MatchInPlayMainMenu
                .MatchPlayMenu
                .TxtScores
                .text = GoalsFoundTemp;


            GraphicsManager.Instance.mobileBtnInputs._Joystick.SetActive(true); // mine

            //GraphicsManager.Instance
            //    .GameOnMainMenu
            //    .MatchInPlayMainMenu
            //    .MatchPlayMenu
            //    .Init(GameManager.Instance.MatchDayMatchSettings.IsRadarOn,
            //    4,
            //    0,
            //    MatchManager.Instance.AwayTeamData.ShortName,
            //    MatchManager.Instance.HomeTeamData.ShortName,
            //    "00:00");

            Debug.Log("Half Exit 2: " + GraphicsManager.Instance
                .GameOnMainMenu
                .MatchInPlayMainMenu
                .MatchPlayMenu
                .TxtScores
                .text);
        }
    }

    //private void Instance_OnGoalScored(string message)
    //{
    //    Debug.Log("Goal: " + message + " Text: " + GraphicsManager.Instance
    //        .GameOnMainMenu
    //        .MatchInPlayMainMenu
    //        .MatchPlayMenu
    //        .TxtScores
    //        .text);

    //    // update the ui
    //    GraphicsManager.Instance
    //        .GameOnMainMenu
    //        .MatchInPlayMainMenu
    //        .MatchPlayMenu
    //        .TxtScores
    //        .text = message;



    //    //message the sound manager
    //    GameManager.Instance.OnGoalScored();
    //}
}
