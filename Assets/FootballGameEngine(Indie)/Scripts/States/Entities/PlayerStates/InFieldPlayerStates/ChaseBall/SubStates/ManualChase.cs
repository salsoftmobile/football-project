﻿using Assets.FootballGameEngine_Indie.Scripts.Entities;
using Assets.FootballGameEngine_Indie.Scripts.StateMachines.Entities;
using Assets.FootballGameEngine_Indie.Scripts.States.Entities.PlayerStates.InFieldPlayerStates.ControlBall.MainState;
using Assets.FootballGameEngine_Indie.Scripts.States.Entities.PlayerStates.InFieldPlayerStates.TacklePlayer.MainState;
using RobustFSM.Base;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.FootballGameEngine_Indie.Scripts.States.Entities.PlayerStates.InFieldPlayerStates.ChaseBall.SubStates
{
    public class ManualChase : BState
    {

        //For Moblie
        [HideInInspector]
        public Vector2 RunAxis;
        public FixedJoystick MoveJoystick;


        bool _updateLogic;

        float _distance;
        float _targetSpeed;
        float _timeToTarget;

        Vector3 _targetPosition;
        Vector3 _targetVelocity;
        Vector3 _targetFuturePosition;

        /// <summary>
        /// The steering target
        /// </summary>
        /// 
        public Button ShortShoot, LongShoot, Shoot;
        public bool _isShortShotBtn = false /*_isLongShotBtn = false, _isShoot = false*/;

        public Vector3 SteeringTarget { get; set; }
        void Start()
        {
            MoveJoystick = GameObject.Find("FixedJoystick").GetComponent<FixedJoystick>();
            ShortShoot = GameObject.Find("ShortShotBtn").GetComponent<Button>();
          //  ShortShoot.onClick.AddListener(() => ShortshotBtn());

            //LongShoot = GameObject.Find("LongShotBtn").GetComponent<Button>();
            //LongShoot.onClick.AddListener(() => LongshotBtn());

            //Shoot = GameObject.Find("BigShoot").GetComponent<Button>();
            //Shoot.onClick.AddListener(() => ShootBtn());

        }
        public override void Enter()
        {
            base.Enter();


            // short shoot
            if (ShortShoot == null)
                ShortShoot = GameObject.Find("ShortShotBtn").GetComponent<Button>();

            ShortShoot.onClick.RemoveAllListeners();
            ShortShoot.onClick.AddListener(() => ShortshotBtn());

            //// long shoot
            //if (LongShoot == null)
            //    LongShoot = GameObject.Find("LongShotBtn").GetComponent<Button>();

            //LongShoot.onClick.AddListener(() => LongshotBtn());


            //// Shoot 
            //if (Shoot == null)
            //    Shoot = GameObject.Find("BigShoot").GetComponent<Button>();

            //Shoot.onClick.AddListener(() => ShootBtn());



            // enable the user controlled icon
            Owner.PlayerControlInfoWidget.Root.SetActive(true);

            // set update logic
            _updateLogic = false;

            //get the steering target
            SteeringTarget = Ball.Instance.NormalizedPosition;

            //set the steering to on
            Owner.RPGMovement.SetMoveTarget(SteeringTarget);
            Owner.RPGMovement.SetRotateFacePosition(SteeringTarget);
        }

        //private void Update()
        //{
        //    RunAxis = MoveJoystick.input;
        //}


        //public void LongshotBtn()
        //{
        //    Debug.Log("its Long");
        //    _isLongShotBtn = true;
        //}
        public void ShortshotBtn()
        {
            if (_isShortShotBtn)
                return;
            Debug.Log("its Short");
            _isShortShotBtn = true;
        }
        //public void ShootBtn()
        //{
        //    Debug.Log("its Big Shoot");
        //    _isShoot = true;
        //}


        public override void Execute()
        {
            base.Execute();

            if (MoveJoystick == null)
                MoveJoystick = GameObject.Find("FixedJoystick").GetComponent<FixedJoystick>();
            // update logic
            if (_updateLogic)
            {
                //check if ball is within control distance
                if (Ball.Instance.CurrentOwner != null
                    && Ball.Instance.CurrentOwner.IsBallWithinControllableDistance()
                    && Owner.IsBallWithinControllableDistance())
                {
                    //tackle player
                    SuperMachine.ChangeState<TackleMainState>();
                }
                else if (Owner.IsBallWithinControllableDistance())
                {
                    // control ball
                    SuperMachine.ChangeState<ControlBallMainState>();
                }

                // calculate the right pos to steer to
                _timeToTarget = Owner.TimeToTarget(Owner.Position, Ball.Instance.NormalizedPosition, Owner.ActualJogSpeed);
                if (Ball.Instance.CurrentOwner == null)
                {
                    _targetPosition = Ball.Instance.Position;
                    _targetSpeed = Ball.Instance.Rigidbody.velocity.magnitude;
                    _targetVelocity = Ball.Instance.Rigidbody.velocity;
                }
                else
                {
                    _targetPosition = Ball.Instance.CurrentOwner.Position;
                    _targetSpeed = Ball.Instance.CurrentOwner.RPGMovement.Speed;
                    _targetVelocity = Ball.Instance.CurrentOwner.RPGMovement.MovementDirection;
                }

                _distance = _targetSpeed * _timeToTarget * 0.5f;
                _targetFuturePosition = _targetPosition + _targetVelocity.normalized * _distance;

                //get the steering target
                SteeringTarget = _targetFuturePosition;

                //set the steering to on
                Owner.RPGMovement.SetMoveTarget(SteeringTarget);
                Owner.RPGMovement.SetRotateFacePosition(SteeringTarget);
            }
            else
            {

                // listen to move commands
             //   float horizontalRot = Input.GetAxisRaw("Horizontal");
             //   float verticalRot = -Input.GetAxisRaw("Vertical");


                if (MoveJoystick == null)
                    MoveJoystick = GameObject.Find("FixedJoystick").GetComponent<FixedJoystick>();

                float horizontalRot = MoveJoystick.input.x;
               float verticalRot = -MoveJoystick.input.y;
                //   Debug.Log("manual Chaese Axis "+ horizontalRot);



                //calculate the direction to rotate to
                Vector3 input = new Vector3(verticalRot, 0f, horizontalRot);

                // calculate camera relative direction to move:
                Vector3 Movement = input;

                //process if any key down
                if (input == Vector3.zero)
                {
                    if (Owner.RPGMovement.Steer == true)
                        Owner.RPGMovement.SetSteeringOff();

                    if (Owner.RPGMovement.Track == true)
                        Owner.RPGMovement.SetTrackingOff();
                }
                else
                {
                    // set the movement
                    Vector3 moveDirection = Movement == Vector3.zero ? Vector3.zero : Owner.transform.forward;
                    Owner.RPGMovement.SetMoveDirection(moveDirection);
                    Owner.RPGMovement.SetRotateFaceDirection(Movement);


                    bool isSprinting;
                    if (Owner.gameObject.name == "InFieldPlayer (4)")
                    {
                        //isSprinting = !Input.GetButton("Sprint");
                        isSprinting = !GlobleVariable._isfast;
                        Owner.RPGMovement.Speed = isSprinting == false ? Owner.ActualSprintSpeed : Owner.ActualJogSpeed;
                    }
                    else
                    {
                       // isSprinting = !Input.GetButton("Sprint");
                        isSprinting = !GlobleVariable._isfast;
                        Owner.RPGMovement.Speed = isSprinting == true ? Owner.ActualSprintSpeed : Owner.ActualJogSpeed;
                    }
                        // check sprinting
                    //bool isSprinting = !Input.GetButton("Sprint")/*|| GlobleVariable._isfast == false*/;
                    //Owner.RPGMovement.Speed = isSprinting == true ? Owner.ActualSprintSpeed : Owner.ActualJogSpeed;

                        //if (Input.GetButton("Sprint") == false) // mine

                        //{
                        //    isSprinting = false;

                        //}
                    // set the steering to on
                    if (Owner.RPGMovement.Steer == false)
                        Owner.RPGMovement.SetSteeringOn();

                    if (Owner.RPGMovement.Track == false)
                        Owner.RPGMovement.SetTrackingOn();

                    // update animator
                    if (isSprinting == true)
                        Owner.Animator.SetFloat("Forward", Owner.SprintAnimatorValue, 0.1f, 0.1f * Time.deltaTime);
                    else
                        Owner.Animator.SetFloat("Forward", 0.5f, 0.1f, 0.1f * Time.deltaTime);


                    
                }
            }

            // listen to key events
            if (/*Input.GetButton("ShortPass/Press")*/ _isShortShotBtn == true)
            {
             //   GlobleVariable._isfast = true;
                // set update logic
                if (_updateLogic == false)
                    _updateLogic = true;
              
                _isShortShotBtn = false;

                bool isSprinting;
                if (Owner.gameObject.name == "InFieldPlayer (4)")
                {
                    //isSprinting = Input.GetButton("Sprint");
                    isSprinting = GlobleVariable._isfast;
                    Owner.RPGMovement.Speed = isSprinting == false ? Owner.ActualSprintSpeed : Owner.ActualJogSpeed;
                }
                else
                {
                    //isSprinting = Input.GetButton("Sprint");
                    isSprinting = GlobleVariable._isfast;
                    Owner.RPGMovement.Speed = isSprinting == true ? Owner.ActualSprintSpeed : Owner.ActualJogSpeed;
                }

                // check sprinting
                //bool isSprinting = Input.GetButton("Sprint") /*|| GlobleVariable._isfast == false*/;
                //Owner.RPGMovement.Speed = isSprinting == true ? Owner.ActualSprintSpeed : Owner.ActualJogSpeed;

                // set steering
                if (Owner.RPGMovement.Steer == false)
                    Owner.RPGMovement.SetSteeringOn();
                if (Owner.RPGMovement.Track == false)
                    Owner.RPGMovement.SetTrackingOn();

                //if (Input.GetButton("Sprint") == false) // mine

                //{
                //    isSprinting = false;

                //}
                // update animator
                if (isSprinting == true)
                    Owner.Animator.SetFloat("Forward", Owner.SprintAnimatorValue, 0.1f, 0.1f * Time.deltaTime);
                else
                    Owner.Animator.SetFloat("Forward", 0.5f, 0.1f, 0.1f * Time.deltaTime);

                //  _isShortShotBtn = false;
            }
            //else if (Input.GetButtonUp("ShortPass/Press"))
            //{
            //    // set update logic
            //    _updateLogic = false;

            //    // set steering
            //    Owner.RPGMovement.SetSteeringOff();
            //    Owner.RPGMovement.SetTrackingOff();
            //}
        }

        public override void Exit()
        {
            base.Exit();

            // disable the user controlled icon
            Owner.PlayerControlInfoWidget.Root.SetActive(false);
          
            //set the steering to on
            Owner.RPGMovement.SetSteeringOff();
            Owner.RPGMovement.SetTrackingOff();
            // _isLongShotBtn = false;
            _isShortShotBtn = false;
            //   _isShoot = false;
            // reset the animator
            Owner.Animator.SetFloat("Forward", 0f);

            //GlobleVariable._isfast = false;

            
        }

        public Player Owner
        {
            get
            {
                return ((InFieldPlayerFSM)SuperMachine).Owner;
            }
        }
    }
}
