﻿using Assets.FootballGameEngine_Indie.Scripts.Entities;
using Assets.FootballGameEngine_Indie.Scripts.StateMachines.Entities;
using Assets.FootballGameEngine_Indie.Scripts.States.Entities.PlayerStates.InFieldPlayerStates.KickBall.MainState;
using Assets.FootballGameEngine_Indie.Scripts.Utilities.Enums;
using Assets.FootballGameEngine_Indie_.Scripts.Entities;
using RobustFSM.Base;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace Assets.FootballGameEngine_Indie.Scripts.States.Entities.PlayerStates.InFieldPlayerStates.ControlBall.SubStates
{


    public class ManualControl : BState/*,IPointerDownHandler,IPointerUpHandler*/
    {
        Pass _pass;
        Shot _shot;
        Transform _refObject;                 // A reference to the main camera in the scenes transform

        //For Moblie
        [HideInInspector]
        public Vector2 RunAxis;
        public FixedJoystick MoveJoystick;

        public Button ShortShoot,LongShoot,Shoot, FastMove;
        public bool _isShortShotBtn = false, _isLongShotBtn=false,_isShoot=false;


      //  public bool forSprint = false;
      //public bool isSprinting;

        //int Fastmovement = 0;

        void Start()
        {
            MoveJoystick = GameObject.Find("FixedJoystick").GetComponent<FixedJoystick>();
            ShortShoot = GameObject.Find("ShortShotBtn").GetComponent<Button>();
          //  ShortShoot.onClick.AddListener(() => ShortshotBtn());

            LongShoot = GameObject.Find("LongShotBtn").GetComponent<Button>();
          //  LongShoot.onClick.AddListener(() => LongshotBtn());

            Shoot = GameObject.Find("BigShoot").GetComponent<Button>();
        //    Shoot.onClick.AddListener(() => ShootBtn());

           FastMove = GameObject.Find("Run").GetComponent<Button>();
         //   FastMove.onClick.AddListener(() => RunBtn());

           // FastMove.OnPointerDown.AddListener(() => RunBtn());
        }
        //private void Update()
        //{
        //    RunAxis = MoveJoystick.input;
        //    Debug.Log("Run Axis "+ RunAxis);
        //}
        

        public override void Enter()
        {
            base.Enter();
            Debug.Log("Enter Fun");
         // short shoot
            if (ShortShoot == null)
                ShortShoot = GameObject.Find("ShortShotBtn").GetComponent<Button>();

            ShortShoot.onClick.RemoveAllListeners();
            ShortShoot.onClick.AddListener(() => ShortshotBtn());

            // long shoot
            if (LongShoot == null)
                LongShoot = GameObject.Find("LongShotBtn").GetComponent<Button>();

            LongShoot.onClick.RemoveAllListeners();
            LongShoot.onClick.AddListener(() => LongshotBtn());


            // Shoot 
           if (Shoot == null)
                Shoot = GameObject.Find("BigShoot").GetComponent<Button>();

            Shoot.onClick.RemoveAllListeners();
            Shoot.onClick.AddListener(() => ShootBtn());

            // enable the user controlled icon
            Owner.PlayerControlInfoWidget.Root.SetActive(true); 

            // set the animator
            Owner.Animator.SetTrigger("Move");

            if(Owner.gameObject.name == "InFieldPlayer (4)")
            {
                Owner.RPGMovement.Speed = 4f;
                Debug.Log("Faster: " + Owner.gameObject.name);
            }
            
        }
        public void LongshotBtn()
        {
            if (_isLongShotBtn)
                return;
            Debug.Log("its Long");
            _isLongShotBtn = true;
        }
        public void ShortshotBtn()
        {
            if (_isShortShotBtn)
                return;

            Debug.Log("its Short");
            _isShortShotBtn = true;
        }
        public void ShootBtn()
        {
            //if (_isShoot)
            //    return;
            Debug.Log("its Big Shoot");
            _isShoot = true;
        }

        //public void RunBtn()
        //{
        //    Debug.Log("its Run");
        //    Fastmovement = 1;
        //}

       

        public override void Execute()
        {
            base.Execute();
            //if (ShortShoot == null)
            //    ShortShoot = GameObject.Find("ShortShotBtn").GetComponent<Button>();


//ShortShoot.onClick.AddListener(() => LongshotBtn());

//capture input
//float horizontalRot = Input.GetAxisRaw("Horizontal");
//float verticalRot = -Input.GetAxisRaw("Vertical");

            if (MoveJoystick== null)
                MoveJoystick = GameObject.Find("FixedJoystick").GetComponent<FixedJoystick>();

            float horizontalRot = MoveJoystick.input.x;
            float verticalRot = -MoveJoystick.input.y;

            //   Debug.Log("run x"+ horizontalRot);
            //calculate the direction to rotate to
            Vector3 input = new Vector3(verticalRot, 0f, horizontalRot);

            // calculate camera relative direction to move:
            Vector3 Movement = input;

            

            if (/*Input.GetButtonDown("LongPass")*/_isLongShotBtn==true )
            {
                // set the direction of movement
                Vector3 direction = Movement == Vector3.zero ? Owner.transform.forward : Movement;

                // find pass in direction
                bool canPass = Owner.CanLongPassInDirection(direction, out _pass);

                // go to kick ball if can pass
                if (canPass)
                {
                    //go to kick-ball state
                    Owner.KickDecision = KickDecisions.Pass;
                    Owner.Pass = _pass;

                    SuperMachine.ChangeState<KickBallMainState>();
                }
                _isLongShotBtn = false;
            }
            else if (/*Input.GetButtonDown("ShortPass/Press") ||*/ _isShortShotBtn == true)
            {
                //todo::fix short pass logic
                // set the direction of movement
                Debug.Log("Pass");
                Vector3 direction = Movement == Vector3.zero ? Owner.transform.forward : Movement;

                // find pass in direction
                bool canPass = Owner.CanShortPassInDirection(direction, out _pass);

                // go to kick ball if can pass
                if (canPass)
                {
                    //go to kick-ball state
                    Owner.KickDecision = KickDecisions.Pass;
                    Owner.Pass = _pass;

                    SuperMachine.ChangeState<KickBallMainState>();
                  //  _isShortShotBtn = false;
                }
                _isShortShotBtn = false;
            }
            else if (/*Input.GetButtonDown("Shoot")||*/ _isShoot==true)
            {
                Debug.Log("..1");
                //fix shoot logic
                // check if I can score
                bool canScore = Owner.CanScore(out _shot, false, true);

                // shoot if I can score
                if (canScore)
                {
                    Debug.Log("..2");
                    //go to kick-ball state
                    Owner.KickDecision = KickDecisions.Shot;
                    Owner.Shot = _shot;

                    SuperMachine.ChangeState<KickBallMainState>();
                }
                else
                {
                    Debug.Log("..3");
                    // reconsider shot without considering the shot
                    // safety
                    canScore = Owner.CanScore(out _shot, false, false);

                    // shoot if I can score
                    if (canScore)
                    {
                        Debug.Log("..4");
                        //go to kick-ball state
                        Owner.KickDecision = KickDecisions.Shot;
                        Owner.Shot = _shot;

                        SuperMachine.ChangeState<KickBallMainState>();
                    }
                }
                
            }
            else
            {
                Debug.Log("..5");
                //process if any key down
                if (input == Vector3.zero)
                {
                    Debug.Log("..6");
                    // enable direction object
                    if (Owner.PlayerDirectionInfoWidget.Root.activeInHierarchy == true)
                        Owner.PlayerDirectionInfoWidget.Root.SetActive(false);
                    Debug.Log("its 1");
                    if (Owner.RPGMovement.Steer == true)
                        Owner.RPGMovement.SetSteeringOff();
                    Debug.Log("its 2");
                    if (Owner.RPGMovement.Track == true)
                        Owner.RPGMovement.SetTrackingOff();
                }
                else
                {
                    Debug.Log("..7");
                    Debug.Log("its 3");
                    if (Owner.PlayerDirectionInfoWidget.Root.activeInHierarchy == false)
                        Owner.PlayerDirectionInfoWidget.Root.SetActive(true);

                    // set the movement
                    Vector3 moveDirection = Movement == Vector3.zero ? Vector3.zero : Owner.transform.forward;
                    Owner.RPGMovement.SetMoveDirection(moveDirection);
                    Owner.RPGMovement.SetRotateFaceDirection(Movement);

                    // rotate arrow
                    Owner.PlayerDirectionInfoWidget.Root.transform.rotation = Quaternion.Slerp(Owner.PlayerDirectionInfoWidget.Root.transform.rotation, Quaternion.LookRotation(new Vector3(Movement.x, 0f, Movement.z), Vector3.up), Time.deltaTime);

                    // check sprinting
                    //if (forSprint == true)
                    //{
                    //   isSprinting = true;
                    //}
                    //if(Fastmovement == 1)
                    //{
                    //    bool isSprinting = true ;
                    //    Owner.RPGMovement.Speed = isSprinting == true ? Owner.ActualSprintSpeed : Owner.ActualJogSpeed;

                    //    // set the steering to on
                    //    if (Owner.RPGMovement.Steer == false)
                    //        Owner.RPGMovement.SetSteeringOn();

                    //    if (Owner.RPGMovement.Track == false)
                    //        Owner.RPGMovement.SetTrackingOn();

                    //    // update animator
                    //    if (isSprinting == true)
                    //        Owner.Animator.SetFloat("Forward", Owner.SprintAnimatorValue, 0.1f, 0.1f * Time.deltaTime);
                    //    else
                    //        Owner.Animator.SetFloat("Forward", 0.5f, 0.1f, 0.1f * Time.deltaTime);
                    //}
                    //else
                    //{
                    //    Fastmovement = 0;
                    //}
                    // GameObject Test = GameObject.Find("Test");
                    //bool isSprinting = Test.activeInHierarchy;
                    // Debug.Log("sprinting " + isSprinting);
                    bool isSprinting;
                    if (Owner.gameObject.name == "InFieldPlayer (4)") // player name: AE
                    {
                        //isSprinting = !Input.GetButton("Sprint");
                        isSprinting = !GlobleVariable._isfast ;
                        Owner.RPGMovement.Speed = isSprinting == true ? Owner.ActualSprintSpeed : Owner.ActualJogSpeed;
                       
                        Debug.Log("Faster: " + Owner.gameObject.name);
                    }
                    else if (Owner.gameObject.name == "InFieldPlayer (1)") // player name: AB
                    {
                        //isSprinting = !Input.GetButton("Sprint");
                        isSprinting = !GlobleVariable._isfast;
                        Owner.RPGMovement.Speed = isSprinting == false ? Owner.ActualSprintSpeed : Owner.ActualJogSpeed;

                        Debug.Log("Faster: " + Owner.gameObject.name);
                    }

                    else if (Owner.gameObject.name == "InFieldPlayer (8)") // player name: AI
                    {
                        //isSprinting = !Input.GetButton("Sprint");
                        isSprinting = !GlobleVariable._isfast;
                        Owner.RPGMovement.Speed = isSprinting == false ? Owner.ActualSprintSpeed : Owner.ActualJogSpeed;

                        Debug.Log("Faster: " + Owner.gameObject.name);
                    }

                    else
                    {
                        //isSprinting = !Input.GetButton("Sprint");
                        isSprinting = !GlobleVariable._isfast;
                        Owner.RPGMovement.Speed = isSprinting == true ? Owner.ActualSprintSpeed : Owner.ActualJogSpeed;
                    }

                  //  bool isSprinting = !Input.GetButton("Sprint") /*|| GlobleVariable._isfast == false*/ ;
                  //  Owner.RPGMovement.Speed = isSprinting == true ? Owner.ActualSprintSpeed : Owner.ActualJogSpeed;

                  //  set the steering to on
                    if (Owner.RPGMovement.Steer == false)
                        Owner.RPGMovement.SetSteeringOn();

                    if (Owner.RPGMovement.Track == false)
                        Owner.RPGMovement.SetTrackingOn();

                   // update animator
                    if (isSprinting == true)
                        Owner.Animator.SetFloat("Forward", Owner.SprintAnimatorValue, 0.1f, 0.1f * Time.deltaTime);
                    else
                        Owner.Animator.SetFloat("Forward", 0.5f, 0.1f, 0.1f * Time.deltaTime);
                }
            }
        }
        
        public override void Exit()
        {
            base.Exit();

            // disable the user controlled icon
            if (Owner.PlayerDirectionInfoWidget.Root.activeInHierarchy == true)
                Owner.PlayerDirectionInfoWidget.Root.SetActive(false);
            Owner.PlayerControlInfoWidget.Root.SetActive(false);
            _isLongShotBtn = false;
            _isShortShotBtn = false;
            _isShoot = false;
            //stop steering
            Owner.RPGMovement.SetSteeringOff();
            Owner.RPGMovement.SetTrackingOff();

           // GlobleVariable._isfast = false;
        }

        //public void OnPointerDown(PointerEventData eventData)
        //{
        //    forSprint = true;
        //    throw new System.NotImplementedException();
        //}

        //public void OnPointerUp(PointerEventData eventData)
        //{
        //    forSprint = false;
        //    throw new System.NotImplementedException();
        //}
        public Player Owner
        {
            get
            {
                return ((InFieldPlayerFSM)SuperMachine).Owner;
            }
        }
    }
}
