﻿using UnityEngine;
using Assets.FootballGameEngine_Indie.Scripts.Entities;

using Assets.SimpleSteering.Scripts.Movement;



namespace Assets.FootballGameEngine_Indie.Scripts.States.Entities.PlayerStates.InFieldPlayerStates.ChaseBall.SubStates
{

    //[RequireComponent(typeof(RPGMovement))]
    //[RequireComponent(typeof(SupportSpot))]
    public class MyScript : MonoBehaviour
    {
        public FixedJoystick MoveJoystick;
        //[SerializeField]
        public ManualChase manualChase;

        private void Start()
        {
          
        }

        private void Update()
        {
          manualChase.RunAxis = MoveJoystick.input;
        }
    }
}